+++
title = "About Me"
date = ""
author = ""
+++

My name is `Mahesh Malekar`, I'm creator of Bot X which include following bots:

- [Google Drive X](https://tx.me/GdriveXbot)
- [Torrent X](https://tx.me/torrentxbot)
- [URL Uploader X](https://tx.me/url_uploadbot)
- [Caption Editor Bot](https://tx.me/captioneditorbot)
- [Link Generator Bot](https://tx.me/linkgenerator_dl_bot)
- [AIO Uploader Bot](https://tx.me/aiouploaderbot)

To be honest, I'm learning Python through YouTube videos and some articles found on Google. And implementing that knowledge to develop Telegram bots.
Because **“Education without application is just entertainment.”** quote by Tim Sanders
